# DnLabs: Aluminium Cucumber

## Configuration
1. Create `settings.py` file as is in example:
    ```python
    import os
 
    DB_USER = os.environ.get("ALUMINUM_CUCUMBER_DB_USER", "postgres")
    DB_PWD = os.environ.get("ALUMINUM_CUCUMBER_DB_PWD", "postgres")
    DB_HOST = os.environ.get("ALUMINUM_CUCUMBER_DB_HOST", "db-pg")
    DB_NAME = os.environ.get("ALUMINUM_CUCUMBER_DB_NAME", "postgres")
    DB_PORT = os.environ.get("ALUMINUM_CUCUMBER_DB_PORT", "5432")
    ```
    |Parameter name | Env parameter name | Description | Default value |
    |-------|-------------|-------------|-----|
    | DB_USER | ALUMINUM_CUCUMBER_DB_USER | Postgres database user | postgres |
    | DB_PWD | ALUMINUM_CUCUMBER_DB_PWD | Postgres database password | postgres |
    | DB_HOST | ALUMINUM_CUCUMBER_DB_HOST | Postgres database host | db-pg |
    | DB_NAME | ALUMINUM_CUCUMBER_DB_NAME | Postgres database name | postgres |
    | DB_PORT | ALUMINUM_CUCUMBER_DB_PORT | Postgres database port | 5432 |
        
        Important!
            Resolution order for parameters is 
            1. Value from env
            2. Value from settings file
            3. Default value`
  
2. Setup app
    ```bash
    pip install .
    ```

3. Patch database
    ```bash
    aluminium-cucumber-patch-db --settings settings.py
    ```

## Marshalling cucumber json reports to database
* From local file
    ```bash 
    aluminium-cucumber --settings settings.py \ 
                          --driver file \
                          --reason-uri local \
                          --source cucumber.json
    ```
* From http resource
    ```bash
    aluminium-cucumber --settings settings.py \
                          --driver http \
                          --reason-uri local \
                          --source http://localhost/cucumber.json
    ```

## Configuration for development

1. Create `settings.py` file as is in example:
    ```python
    import os
 
    DB_USER = os.environ.get("ALUMINUM_CUCUMBER_DB_USER", "postgres")
    DB_PWD = os.environ.get("ALUMINUM_CUCUMBER_DB_PWD", "system")
    DB_HOST = os.environ.get("ALUMINUM_CUCUMBER_DB_HOST", "db-pg")
    DB_NAME = os.environ.get("ALUMINUM_CUCUMBER_DB_NAME", "postgres")
    DB_PORT = os.environ.get("ALUMINUM_CUCUMBER_DB_PORT", "5432")
    ```
2. Setup conda environment
    ```bash
    conda env --file conda.yml
    ```
    or update existent
    ```bash
    conda env update --file conda.yml
    ```
3. Activate conda
    ```bash
    activate aluminium-cucumber
    ```
4. Install app in development mode
    ```bash
    pip install -e .
    ```
5. Apply database migrations:
    ```bash
    aluminium-cucumber-patch-db --settings settings.py
    ```