"""empty message

Revision ID: 6983bb1fd275
Revises: 
Create Date: 2019-06-02 22:23:40.066134

"""
import sqlalchemy.dialects.postgresql
import sqlalchemy.orm
from alembic import op

from dn.aluminium_cucumber.lib import AluminiumCucumberBase, AluminiumCucumberBaseResource, SecureUuid

# revision identifiers, used by Alembic.
revision = '6983bb1fd275'
down_revision = None
branch_labels = None
depends_on = None


class Tag(AluminiumCucumberBase, AluminiumCucumberBaseResource):
    __incomplete_tablename__ = "tag"
    name = sqlalchemy.Column(sqlalchemy.VARCHAR)

    __table_args__ = (
        sqlalchemy.UniqueConstraint(name),
    )

    @staticmethod
    def get_or_create(session, name):
        try:
            return session.query(Tag).filter_by(name=name).one()
        except sqlalchemy.orm.exc.NoResultFound:
            # noinspection PyArgumentList
            tag = Tag(name=name)
            session.add(tag)

        return tag


class Feature(AluminiumCucumberBase, AluminiumCucumberBaseResource):
    __incomplete_tablename__ = "feature"

    id = sqlalchemy.Column(sqlalchemy.VARCHAR)
    uri = sqlalchemy.Column(sqlalchemy.VARCHAR, nullable=False)
    name = sqlalchemy.Column(sqlalchemy.VARCHAR)
    description = sqlalchemy.Column(sqlalchemy.VARCHAR, nullable=True)

    __table_args__ = (
        sqlalchemy.UniqueConstraint(uri, name),
    )

    @staticmethod
    def get_or_create(session, name: str, description: str, uri: str):
        try:
            return session.query(Feature).filter_by(name=name).one()
        except sqlalchemy.orm.exc.NoResultFound:
            # noinspection PyArgumentList
            feature = Feature(name=name, uri=uri, description=description)
            session.add(feature)
            return feature


class FeatureTag(AluminiumCucumberBase, AluminiumCucumberBaseResource):
    __incomplete_tablename__ = "feature_tag"

    tag = sqlalchemy.Column(SecureUuid, sqlalchemy.ForeignKey(Tag.uuid))
    feature = sqlalchemy.Column(SecureUuid, sqlalchemy.ForeignKey(Feature.uuid))

    __table_args__ = (
        sqlalchemy.UniqueConstraint(tag, feature),
    )

    @staticmethod
    def get_or_create(session, feature: Feature, name):
        tag = Tag.get_or_create(session, name)

        try:
            return session.query(FeatureTag).filter_by(tag=tag.uuid, feature=feature.uuid).one()
        except sqlalchemy.orm.exc.NoResultFound:
            # noinspection PyArgumentList
            feature_tag = FeatureTag(tag=tag.uuid, feature=feature.uuid)
            session.add(feature_tag)

        return feature_tag


class Run(AluminiumCucumberBase, AluminiumCucumberBaseResource):
    __incomplete_tablename__ = "run"
    uri = sqlalchemy.Column(sqlalchemy.VARCHAR, nullable=False)
    reason_uri = sqlalchemy.Column(sqlalchemy.VARCHAR)
    version = sqlalchemy.Column(sqlalchemy.Integer, default=0)

    __table_args__ = (
        sqlalchemy.UniqueConstraint(uri, reason_uri),
    )

    @staticmethod
    def create(session, uri: str, reason_uri: str):
        try:
            session.query(Run).filter_by(uri=uri, reason_uri=reason_uri).one()

            raise Exception("Run is already parsed")
        except sqlalchemy.orm.exc.NoResultFound:
            prev_run = session.query(Run).filter_by(uri=uri).order_by(Run.version.desc()).first()
            if prev_run:
                # noinspection PyArgumentList
                run = Run(uri=uri, reason_uri=reason_uri, version=prev_run.version + 1)
            else:
                # noinspection PyArgumentList
                run = Run(uri=uri, reason_uri=reason_uri)

            session.add(run)

        return run


class Scenario(AluminiumCucumberBase, AluminiumCucumberBaseResource):
    __incomplete_tablename__ = "scenario"
    run = sqlalchemy.Column(SecureUuid, sqlalchemy.ForeignKey(Run.uuid))
    order = sqlalchemy.Column(sqlalchemy.BigInteger)
    sys_name = sqlalchemy.Column(sqlalchemy.VARCHAR)
    name = sqlalchemy.Column(sqlalchemy.VARCHAR)
    description = sqlalchemy.Column(sqlalchemy.VARCHAR)
    feature = sqlalchemy.Column(SecureUuid, sqlalchemy.ForeignKey(Feature.uuid))

    __table_args__ = (
        sqlalchemy.UniqueConstraint(feature, run, order, name),
    )

    @staticmethod
    def get_or_create(session,
                      run: Run,
                      feature: Feature,
                      name: str,
                      order: int,
                      description: str,
                      sys_name: str = None):
        try:
            return session.query(Scenario).filter_by(feature=feature.uuid,
                                                     run=run.uuid,
                                                     order=order,
                                                     name=name).one()
        except sqlalchemy.orm.exc.NoResultFound:
            # noinspection PyArgumentList
            scenario = Scenario(feature=feature.uuid,
                                run=run.uuid,
                                sys_name=sys_name,
                                name=name,
                                order=order,
                                description=description)
            session.add(scenario)

        return scenario

    @staticmethod
    def get_previous(session, run: Run, feature: Feature, scenario):
        scenario = session.query(Scenario) \
            .join(Feature) \
            .join(Run) \
            .filter(Run.uri == run.uuid) \
            .filter(Run.uuid != run.uuid) \
            .filter(Feature == feature.uuid) \
            .filter(Scenario.order == scenario.order) \
            .order_by(Run.version.desc()) \
            .first()

        return scenario, [
            {"stage": step.stage,
             "status": step.status,
             "duration": step.duration,
             "keyword": step.keyword,
             "order": step.order,
             "name": step.name}
            for step in session.query(Step).filter(Step.scenario == scenario.uuid).order_by(Step.order)
        ] if scenario is not None else []


class ScenarioTag(AluminiumCucumberBase, AluminiumCucumberBaseResource):
    __incomplete_tablename__ = "scenario_tag"

    tag = sqlalchemy.Column(SecureUuid, sqlalchemy.ForeignKey(Tag.uuid))
    scenario = sqlalchemy.Column(SecureUuid, sqlalchemy.ForeignKey(Scenario.uuid))

    __table_args__ = (
        sqlalchemy.UniqueConstraint(tag, scenario),
    )

    @staticmethod
    def get_or_create(session, scenario: Scenario, name):
        tag = Tag.get_or_create(session, name)

        try:
            return session.query(ScenarioTag).filter_by(tag=tag.uuid, scenario=scenario.uuid).one()
        except sqlalchemy.orm.exc.NoResultFound:
            # noinspection PyArgumentList
            scenario_tag = ScenarioTag(tag=tag.uuid, scenario=scenario.uuid)
            session.add(scenario_tag)

        return scenario_tag


class Step(AluminiumCucumberBase, AluminiumCucumberBaseResource):
    __incomplete_tablename__ = "step"

    scenario = sqlalchemy.Column(SecureUuid, sqlalchemy.ForeignKey(Scenario.uuid))
    stage = sqlalchemy.Column(sqlalchemy.VARCHAR)
    order = sqlalchemy.Column(sqlalchemy.BigInteger)
    keyword = sqlalchemy.Column(sqlalchemy.VARCHAR)
    name = sqlalchemy.Column(sqlalchemy.VARCHAR)
    status = sqlalchemy.Column(sqlalchemy.VARCHAR)
    duration = sqlalchemy.Column(sqlalchemy.BigInteger, nullable=False)

    __table_args__ = (
        sqlalchemy.UniqueConstraint(scenario, order),
    )


_TABLES = [Run, Tag, Feature, FeatureTag, Scenario, ScenarioTag, Step]


def upgrade():
    bind = op.get_bind()

    for table in _TABLES:
        table.__table__.create(bind)


def downgrade():
    for table in reversed(_TABLES):
        op.drop_table(table.__tablename__)
