import uuid
import time
import sqlalchemy.dialects.postgresql
import sqlalchemy.ext.declarative


def uuid4() -> str:
    return str(uuid.uuid4())


def linux_now() -> int:
    return int(time.time())


SecureUuid = sqlalchemy.dialects.postgresql.UUID().with_variant(sqlalchemy.VARCHAR, "sqlite")

Base = sqlalchemy.ext.declarative.declarative_base()


class AluminiumCucumberBaseResource:
    uuid = sqlalchemy.Column(SecureUuid, primary_key=True, default=uuid4)
    created_at = sqlalchemy.Column(sqlalchemy.BigInteger, default=linux_now)
    updated_at = sqlalchemy.Column(sqlalchemy.BigInteger, default=linux_now, onupdate=linux_now)


class AluminiumCucumberBase(Base):

    __abstract__ = True

    _the_prefix = 'aluminium_cucumber_'
    __incomplete_tablename__ = "dummy"

    @sqlalchemy.ext.declarative.declared_attr
    def __tablename__(self):
        return self._the_prefix + self.__incomplete_tablename__
