from .basic import Loader
from .file import FileLoader
from .http import HttpLoader
