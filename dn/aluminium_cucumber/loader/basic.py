import logging

import sqlalchemy.orm

from ..db import Run, Feature, ScenarioTag, FeatureTag, Scenario, Step


class Loader:
    def __init__(self, cfg):
        self.log = logging.getLogger(self.__class__.__name__)

        self.engine = sqlalchemy.create_engine(cfg.DB_URI)
        self.Session = sqlalchemy.orm.sessionmaker(bind=self.engine)

    def __enter__(self):
        self.session = self.Session()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.session.flush()
        self.session.commit()
        self.session.close()

        del self.session

    def load(self, data: dict, uri: str, reason_uri: str):
        run = Run.create(self.session, uri, reason_uri)
        self.log.info(f"Processing {run.uri} due to {run.reason_uri}")

        for feature_definition in data:
            self.log.info(f"    Processing feature {feature_definition['name']}")
            kwargs = {
                "name": feature_definition['name'],
                "uri": feature_definition['uri'],
            }
            if 'description' in feature_definition.keys():
                kwargs['description'] = feature_definition['description']

            feature = Feature.get_or_create(self.session, **kwargs)
            if 'tags' in feature_definition.keys():
                for tag_definition in feature_definition['tags']:
                    FeatureTag.get_or_create(self.session, feature, tag_definition['name'])

            steps = []
            scenario_no = 0

            for scenario_definition in feature_definition['elements']:
                self.log.info(f"        Processing scenario {scenario_definition['name'] or 'before'}")
                for step in scenario_definition['steps']:
                    steps.append({"stage": scenario_definition["type"],
                                  "status": step["result"]["status"],
                                  "duration": step["result"]["duration"] if "duration" in step["result"].keys() else -1,
                                  "keyword": step["keyword"].strip(),
                                  "order": len(steps),
                                  "name": step['name']})

                if scenario_definition['type'] == 'scenario':
                    scenario = Scenario.get_or_create(self.session,
                                                      run,
                                                      feature,
                                                      scenario_definition['name'],
                                                      scenario_no,
                                                      scenario_definition['description'],
                                                      **{"sys_name": scenario_definition['id']}
                                                      if "id" in scenario_definition.keys()
                                                      else {})

                    prev_scenario, prev_steps = Scenario.get_previous(self.session,
                                                                      run,
                                                                      feature,
                                                                      scenario)

                    if 'tags' in scenario_definition.keys():
                        for tag_definition in scenario_definition['tags']:
                            ScenarioTag.get_or_create(self.session, scenario, tag_definition['name'])

                    if prev_steps != steps:
                        for step in steps:
                            # noinspection PyArgumentList
                            self.session.add(Step(scenario=scenario.uuid, **step))
                    steps = []
                    scenario_no += 1
