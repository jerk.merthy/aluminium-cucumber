import json

from .basic import Loader


class FileLoader(Loader):
    def load(self, file_path, *args, **kwargs):
        with open(file_path) as f:
            super().load(json.load(f.read()), *args, **kwargs)
