import requests

from .basic import Loader


class HttpLoader(Loader):
    def load(self, file_url, auth=None, *args, **kwargs):
        r = requests.get(file_url, **{"auth": auth} if auth else {})
        super().load(r.json(), *args, **kwargs)
