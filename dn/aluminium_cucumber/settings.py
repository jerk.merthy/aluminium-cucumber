import os
import sys
import argparse
import importlib.util

DN_APP_NAME = "aluminium_cucumber"

DB_USER = os.environ.get("ALUMINUM_CUCUMBER_DB_USER", "postgres")
DB_PWD = os.environ.get("ALUMINUM_CUCUMBER_DB_PWD", "system")
DB_HOST = os.environ.get("ALUMINUM_CUCUMBER_DB_HOST", "db-pg")
DB_NAME = os.environ.get("ALUMINUM_CUCUMBER_DB_NAME", "postgres")
DB_PORT = os.environ.get("ALUMINUM_CUCUMBER_DB_PORT", "5432")

parser = argparse.ArgumentParser()

parser.add_argument('--settings', required=False)

args, _ = parser.parse_known_args()

if 'settings' in args and args.settings is not None:
    if os.path.isabs(args.settings):
        spec = importlib.util.spec_from_file_location("settings",
                                                      args.settings)
    else:
        spec = importlib.util.spec_from_file_location("settings",
                                                      os.path.join(os.path.dirname(sys.argv[0]), args.settings))
    mod = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(mod)
    for key, value in mod.__dict__.items():
        globals()[key] = value

DB_URI = "postgresql://%s:%s@%s:%s/%s" % (
    DB_USER,
    DB_PWD,
    DB_HOST,
    DB_PORT,
    DB_NAME
)
DB_DSN = 'user=%s password=%s host=%s port=%s dbname=%s application_name=%s' % (
    DB_USER,
    DB_PWD,
    DB_HOST,
    DB_PORT,
    DB_NAME,
    DN_APP_NAME
)
