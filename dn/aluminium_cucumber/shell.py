import argparse
import importlib.util
import os
import sys

import alembic.config


def _check_settings(settings_path):
    if os.path.isabs(settings_path):
        file_path = settings_path
    else:
        file_path = os.path.join(os.path.dirname(sys.argv[0]), settings_path)

    if not os.path.exists(file_path):
        raise FileNotFoundError(f"file {settings_path} does not exists")

    if not os.path.isfile(file_path):
        raise FileNotFoundError(f"path {settings_path} is not a file")

    _, extension = os.path.splitext(file_path)
    if not extension == ".py":
        raise FileNotFoundError(f"file {settings_path} should be of py extension")

    # try import
    try:
        spec = importlib.util.spec_from_file_location("settings", file_path)
        mod = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(mod)
    except Exception as ex:
        raise FileNotFoundError(f"file {settings_path} is not a valid py file") from ex


def main():
    sys.argv[0] = os.path.join(os.getcwd(), 'aluminium-cucumber')

    parser = argparse.ArgumentParser()

    parser.add_argument("--settings", required=True)
    parser.add_argument("--source", required=True)
    parser.add_argument("--driver", choices=["http", "file"], default="file", required=True)
    parser.add_argument("--reason-uri", required=True)

    args, _ = parser.parse_known_args()

    _check_settings(args.settings)

    if args.driver == "http":
        from .loader import HttpLoader as Loader
    elif args.driver == "file":
        from .loader import FileLoader as Loader
    else:
        raise Exception("invalid value for --driver")

    from dn.aluminium_cucumber import settings

    loader = Loader(settings)

    with loader:
        loader.load(args.source, uri=args.source, reason_uri=args.reason_uri)


def patch_db():
    sys.argv[0] = os.path.join(os.getcwd(), 'aluminium-cucumber-patch-db')

    parser = argparse.ArgumentParser()

    parser.add_argument("--settings", required=True)

    args, _ = parser.parse_known_args()

    _check_settings(args.settings)

    os.chdir(os.path.join(os.path.dirname(__file__), "db/"))

    alembic_args = [
        '--raiseerr',
        'upgrade', 'head',
    ]
    alembic.config.main(argv=alembic_args)
