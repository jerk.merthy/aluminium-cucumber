import os

DB_USER = os.environ.get("ALUMINUM_CUCUMBER_DB_USER", "postgres")
DB_PWD = os.environ.get("ALUMINUM_CUCUMBER_DB_PWD", "system")
DB_HOST = os.environ.get("ALUMINUM_CUCUMBER_DB_HOST", "db-pg")
DB_NAME = os.environ.get("ALUMINUM_CUCUMBER_DB_NAME", "postgres")
DB_PORT = os.environ.get("ALUMINUM_CUCUMBER_DB_PORT", "5432")
