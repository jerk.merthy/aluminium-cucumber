from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='dn.aluminium-cucumber',
    version='0.0.0',
    author='Paul Tymoshenko',
    author_email='paul.tymoshenko@gmail.com',
    packages=find_packages(exclude=(
        "settings.py",
    )),
    package_dir={'db/aluminium_cucumber': 'aluminium_cucumber'},
    package_data={'aluminium_cucumber': ['db/*.ini']},

    entry_points={
        "console_scripts": ["aluminium-cucumber = dn.aluminium_cucumber.shell:main",
                            "aluminium-cucumber-patch-db = dn.aluminium_cucumber.shell:patch_db"]
    },
    install_requires=[
        "alembic==1.0.9",
        "psycopg2==2.7.6.1",
        "sqlalchemy==1.3.3",
    ],
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)
